package Client;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.Socket;
import java.net.SocketException;
import java.net.UnknownHostException;

public class Client {
    private String hostname;
    private int porto;
    public Socket socket;
    public BufferedReader in;
    public PrintWriter out;
    BufferedReader systemIn;

    public Client(String hostname, int porto){
        this.hostname = hostname;
        this.porto = porto;
    }

    public void clientStart(){
        try {
            System.out.println("### CLIENT ###");

            System.out.println("> Connecting to server...");
            socket = new Socket(this.hostname, this.porto);
            System.out.println("> Connection accepted");

            //criar canal de leitura do stdin
            systemIn = new BufferedReader(new InputStreamReader(System.in));
            in = new BufferedReader(new InputStreamReader(socket.getInputStream()));
            out = new PrintWriter(socket.getOutputStream());

            System.out.println("\n==== CLIENT ====\n");
            System.out.println("[1] Login");
            System.out.println("[2] Registar");
            System.out.println("[quit] Sair");

            //O cliente só poderá colocar um dos anteriores comandos
            String userInput = systemIn.readLine();
            out.println(userInput);
            switch (userInput) {
                case "1":
                    this.logreg();
                    break;

                case "2":
                    this.logreg();
                    break;

                case "quit":
                    this.quitP();
            }

            // Clt logado em escuta activa e pronto para enviar informação
            if (!userInput.equals("quit") && userInput != null){
                //Criar listener thread para receber mensagens de outros utilizadores
                Thread listener = new Thread(new ClientListener());
                listener.start();

                System.out.println("== COVID-19 ==");
                System.out.println("NOTA: Em qualquer altura pode introduzir o nº de casos seus conhecidos!");
                while((userInput = systemIn.readLine())!=null && !userInput.equals("quit")) {
                    out.println(userInput);
                    out.flush();
                }
            }

            //fechar sockets
            systemIn.close();
            socket.shutdownOutput();
            socket.shutdownInput();
            socket.close();

        } catch (UnknownHostException e) {
            System.out.println("ERRO: Servidor nao encontrado!");

        } catch (IOException e) {
            System.out.println("ERRO: O servidor não está em execução!");
        }

    }

    public void logreg(){
        String response;
        Boolean nok;
        try {
            do{
                System.out.println("Insira Username:");
                String user = systemIn.readLine();
                System.out.println("Insira Password:");
                String pass = systemIn.readLine();
                String userpass = user+"|"+pass;
                out.println(userpass);
                out.flush();
                response = in.readLine();
                if(nok=!response.equals("ok")){
                    System.out.println(response);
                }
            }while(nok);
            System.out.println("Operação efectuada com sucesso.");

        }catch (IOException e){
            e.printStackTrace();
        }
    }

    public void quitP(){
        out.println("quit");
        out.flush();
    }

    public static void main(String[] args){
        Client c = new Client("127.0.0.1", 12345);
//        Client c = new Client("",)
        c.clientStart();
    }

    public class ClientListener implements Runnable{
        public void ClientListner(){}

        public void run(){
            String message;
            try{
                while((message = in.readLine()) != null){
                    System.out.println(message);
                }
            }
            catch (SocketException e){}
            catch (IOException e){
                e.printStackTrace();
            }
        }
    }

}

