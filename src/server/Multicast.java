package Server;

import java.io.PrintWriter;
import Server.Exceptions.*;

public class Multicast implements Runnable{
    private UserList ul;
    private PrintWriter out;
    private int counter;

    public Multicast(UserList ul, PrintWriter out){
        this.ul = ul;
        this.out = out;
    }

    @Override
    public void run() {

        while(true){
            this.ul.readerWait();

            // acorda os readers porque alguem alterou o lastBalance
            ul.readerLock();


            double lBalance = ul.getLastBalance();
            ul.readerUnlock();

            out.println("Estimativa Global: "+lBalance);
            out.flush();
        }
    }
}
