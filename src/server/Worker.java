package Server;

import Server.Exceptions.*;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.Socket;

public class Worker implements Runnable{
    private String username;
    private String password;
    private Server server;
    private Socket socket;
    private BufferedReader in;
    private PrintWriter out;

    public Worker(Server server, Socket socket) throws IOException {
        this.server = server;
        this.socket = socket;
        this.in = new BufferedReader(new InputStreamReader(socket.getInputStream()));
        this.out = new PrintWriter(socket.getOutputStream());
    }

    @Override
    public void run(){
        try{

            // Verifica a opcao que o Cliente escolheu
            String option;
            option = in.readLine();

            boolean flag = false; //ainda nao conseguiu autenticar
            while (!flag && !option.equals("quit")){


                String userpass = in.readLine();
                String [] data = userpass.split("\\|"); // "\\|" escape do caracter especial |
                username = data[0];
                password = data[1];


                if(option.equals("1")){     //LOGIN
                    try {
                        flag = server.loginClient(username, password, out);
                    } catch (UserAlreadyConnectedException e) {
                        System.out.println("catch already connected");
                        out.println("User already connected");
                        out.flush();
                    } catch (UserLoginInvalidException e) {
                        System.out.println("catch invalid login");
                        out.println("Wrong username or password");
                        out.flush();
                    }
                    if(flag){
                        //O utilizador fez login com sucesso
                        out.println("ok");
                        out.flush();

                        double balance = server.getClientList().getLastBalance();
                        out.println("Estimativa Global: "+balance);
                        out.flush();
                    }
                }else if(option.equals("2")) {      //REGISTER
                    try {
                        flag = server.registerClient(username, password, out);
                    }catch (UserAlreadyConnectedException e){
                        out.println("User already connected!");
                        out.flush();
                    }catch (UserAlreadyRegisteredException e){
                        out.println("User already registered!");
                        out.flush();
                    }
                    if(flag){
                        //Fez registo com sucesso
                        out.println("ok");
                        out.flush();

                        double balance = server.getClientList().getLastBalance();
                        out.println("Estimativa Global: "+balance);
                        out.flush();
                    }
                }
            }

            if(!option.equals("quit")){

                //Thread em escuta activa
                Thread t = new Thread(new Multicast(server.getClientList(), out));
                t.start();

                //receber mensagens do utilizador e difundir pelos restantes clientes
                String msg = null;
                //Thread em escuta activa
                double valor = 0;
                while ( (msg = in.readLine()) != null && !(msg.equals("quit")) ) {
                    try {
                        valor = Double.parseDouble(msg);
                    }catch (NumberFormatException e){
                        out.println("Input nao valido!");
                    }

                    server.getClientList().updateBalance(username,valor);
                }
            }


            // remover cliente da lista de clientes conectados
            server.disconnectClient(username);
            System.out.println("Client: "+username +" disconnected.");


            //fechar sockets
            socket.shutdownInput();
            socket.shutdownOutput();
            socket.close();

        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
