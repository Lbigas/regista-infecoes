package Server;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.ServerSocket;
import java.net.Socket;
import Server.Exceptions.*;

public class ServerMain {
    private ServerSocket serverSocket;
    private int porto;
    private Server server;
    public BufferedReader termIn;

    public ServerMain(int porto) throws IOException {
        this.porto = porto;
        this.server = new Server();
    }

    public void startServer(){
        try{
            System.out.println("### SERVER ###");
            this.serverSocket = new ServerSocket(this.porto);

            Thread sIn = new Thread(new ServerIn(this.server));
            sIn.start();

            while(true){
                System.out.println("ServerMain > Server is running waiting for a new connection...");
                Socket socket = serverSocket.accept();
                System.out.println("ServerMain > Connection received! Create worker thread to handle connection");
                Thread t = new Thread(new Worker(this.server,socket));
                t.start();
            }
        } catch (IOException e) {
            System.out.println("Error accepting connection: " + e.getMessage());
        }
    }

    public static void main(String[] args) throws IOException{
        new ServerMain(12345).startServer();
    }
}
