package Server;

import java.util.concurrent.locks.Condition;
import java.util.concurrent.locks.ReentrantLock;
import Server.Exceptions.*;

public class Lock {
    private ReentrantLock lock;
    private Condition writerCondition;
    private Condition readerConditon;
    private boolean writing;
    private int reading;
    private int readerwait;

    public Lock() {
        this.lock = new ReentrantLock();
        this.writerCondition = this.lock.newCondition();
        this.readerConditon = this.lock.newCondition();

        this.writing = false;
        this.reading = 0;

        this.readerwait = 0;
    }

    public void readerWait(){
        this.lock.lock();
        try {
            this.readerConditon.await();
        }catch (InterruptedException e){
            e.printStackTrace();
        }
        this.lock.unlock();
    }

    public void readerLock(){
        this.lock.lock();
        try{
            this.readerwait += 1;
            while(this.writing){
                this.readerConditon.await();
            }
            this.readerwait -= 1;
            this.reading += 1;
        }
        catch (Exception e){
            e.printStackTrace();
        }
        finally {
            this.lock.unlock();
        }
    }

    public void readerUnlock(){
        this.lock.lock();
        try{
            this.reading -= 1;
            if(this.reading == 0){
                this.writerCondition.signal();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        finally {
            this.lock.unlock();
        }
    }


    public void writerLock(){
        this.lock.lock();
        try{
            //Espera quando existe alguem a ler, alguem a escrever ou alguem à espera de ler
            while(this.reading > 0 || this.writing || this.readerwait > 0){
                this.writerCondition.await();
            }
            //representa uma thread que quer escrever
            this.writing = true;
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        finally{
            this.lock.unlock();
        }
    }

    public void writerUnlock(){
        this.lock.lock();
        try{
            this.writing = false;
            this.readerConditon.signalAll();
            this.writerCondition.signal();
        } catch (Exception e) {
            e.printStackTrace();
        }
        finally {
            this.lock.unlock();
        }
    }
}
