package Server;

import java.util.concurrent.locks.ReentrantLock;
import Server.Exceptions.*;

public class User extends ReentrantLock {
    private String password;
    double cases;
    boolean updated;   // true se já introduziu alguma estatistica [default: false]

    public User() {
        this.password = "";
        this.cases = 0;
        this.updated = false;
    }

    public void setPassword(String pass){
        this.password = pass;
    }

    public void setCases(double cases){
        this.cases = cases;
    }

    public void setUpdated(boolean up){
        this.updated = up;
    }

    public String getPassword(){
        return this.password;
    }

    public double getCases(){
        return this.cases;
    }

    public boolean getUpdated(){
        return this.updated;
    }

    public boolean checkPassword(String pass){
        if(this.password.equals(pass)){
            return true;
        }
        else{
            return false;
        }
    }
}
