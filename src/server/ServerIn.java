package Server;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import Server.Exceptions.*;

public class ServerIn implements Runnable{

    private Server server;

    public ServerIn(Server server){
        this.server = server;
    }

    @Override
    public void run() {

        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));

        while(true){
            try {
                String read = reader.readLine();
                System.out.println("Saving");
                this.parseInput(read);
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    public void parseInput(String in) throws IOException {
        if(in.equals("save")){
            this.server.saveUserData();
        }
    }
}
