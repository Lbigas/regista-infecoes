package Server;

import Server.Exceptions.*;

import java.io.*;
import java.util.HashMap;
import java.util.concurrent.locks.ReentrantLock;


public class UserList extends ReentrantLock {

    private Lock lockUsrList;

    private HashMap<String,User> users;
    private double lastBalance;
    private int nRegisters;         // quantos registos são válidos para o cálculo do balanço final

    public UserList(){
        this.lockUsrList = new Lock();
        this.users = new HashMap<>();
        this.lastBalance = 0;
    }

    public boolean addUser(String name, String password) {
        this.lockUsrList.readerLock();
        if(this.users.containsKey(name)){
            this.lockUsrList.readerUnlock();
            return false;
        }

        else{
            this.lockUsrList.readerUnlock();
            this.lockUsrList.writerLock();
            User user = new User();
            user.setPassword(password);
            this.users.put(name, user);
            this.lockUsrList.writerUnlock();
            return true;
        }
    }

    public boolean checkLogin(String username, String password) {
        this.lockUsrList.readerLock();
        try {
            User user = this.getUser(username);
            if(user.checkPassword(password)){
                return true;
            }
            else{
                return false;
            }
        } catch (UserDoesNotExistException e) {
            return false;
        }
        finally {
            this.lockUsrList.readerUnlock();
        }
    }

    public User getUser(String name) throws UserDoesNotExistException {
        if (!this.users.containsKey(name)) {
            throw new UserDoesNotExistException(name);
        } else {
            return this.users.get(name);
        }
    }

    public boolean userExists(String name) {
        if(this.users.containsKey(name)){
            return true;
        }
        return false;
    }

    public void calcBalance() {
        double sum = 0;
        double num = 0;
        for(String n : this.users.keySet()){
            try {
                User u = this.getUser(n);
                if (u.updated) {
                    num += 1;
                    sum = sum + (u.getCases());
                }
            }
            catch (UserDoesNotExistException e) {
                e.printStackTrace();
            }
        }

        if(num == 0){
            this.lastBalance = 0;
            this.nRegisters = (int) num;
        }

        else {
            double ret = (sum / 150) / num;
            this.lastBalance = ret;
            this.nRegisters = (int) num;
        }
    }

    public void updateBalance (String user, double valor){
        this.lockUsrList.writerLock();
        //calcular proporção [nº casos conhecidos / nº contactos
        double prop =  valor/150;

        User usr = this.users.get(user);

        if(usr.getUpdated()==false) {     // primeira estimativa dada pelo user
            usr.setCases(valor);
            usr.setUpdated(true);
            nRegisters++;
            lastBalance = (lastBalance+prop)/nRegisters;
        }
        else{
            double old = usr.getCases()/150;
            usr.setCases(valor);
            double sumAllcases = lastBalance * nRegisters;
            double newSum = sumAllcases - old + prop;
            lastBalance = newSum / nRegisters;
        }

        this.lockUsrList.writerUnlock();
    }

    public double getLastBalance() {
        return this.lastBalance;
    }

    public void readerWait(){
        this.lockUsrList.readerWait();
    }

    public void lockW(){
        this.lockUsrList.writerLock();
    }

    public void readerLock(){
        this.lockUsrList.readerLock();
    }

    public void unlockW(){
        this.lockUsrList.writerUnlock();
    }

    public void readerUnlock(){
        this.lockUsrList.readerUnlock();
    }



    // Para recuperar/guardar informacoes do ficheiro temos de bloquear tudo,
    // nao podem ocorrer alteracoes ao sistema enquanto se escreve para o sistema,
    // Nao podemos permitir que ocorramvarias escitas para o fichjeiro ao mesmo tempo,
    // tal ocorrencia levaria a corrupcao do ficheiro.
    public synchronized void LoadLocalData() throws SaveFileDoesNotExistException{
        try{
            FileInputStream fis = new FileInputStream("UserData");
            ObjectInputStream ois = new ObjectInputStream(fis);
            this.users = (HashMap<String, User>) ois.readObject();
            ois.close();
        }
        catch(FileNotFoundException e){
            throw new SaveFileDoesNotExistException();
        } catch (IOException e) {
            e.printStackTrace();
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }
    }

    public synchronized void SaveLocalData() throws IOException {
        FileOutputStream fos = new FileOutputStream("UserData");
        ObjectOutputStream oos = new ObjectOutputStream(fos);
        oos.writeObject(this.users);
        oos.flush();
        oos.close();
    }

    public int updateNRegisters() throws UserDoesNotExistException {
        int ret = 0;
        for(String n : this.users.keySet()){
            User u = this.getUser(n);
            if(u.updated) {
                ret += 1;
            }
        }

        return ret;
    }
}
