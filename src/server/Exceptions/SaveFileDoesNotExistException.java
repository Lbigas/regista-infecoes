package Server.Exceptions;

public class SaveFileDoesNotExistException extends Exception{
    public SaveFileDoesNotExistException(){
        super();
    }

    public SaveFileDoesNotExistException(String msg) {
        super(msg);
    }
}
