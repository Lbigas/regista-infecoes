package Server.Exceptions;

public class UserLoginInvalidException extends Exception {
    public UserLoginInvalidException(){
        super();
    }

    public UserLoginInvalidException(String msg) {
        super(msg);
    }
}
