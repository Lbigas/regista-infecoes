package Server.Exceptions;

public class UserAlreadyConnectedException extends Exception {
    public UserAlreadyConnectedException(){
        super();
    }

    public UserAlreadyConnectedException(String msg) {
        super(msg);
    }
}
