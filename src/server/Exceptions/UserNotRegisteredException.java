package Server.Exceptions;

public class UserNotRegisteredException extends Exception{

    public UserNotRegisteredException(){
        super();
    }

    public UserNotRegisteredException(String msg) {
        super(msg);
    }

}
