package Server.Exceptions;

public class UserDoesNotExistException extends Exception{
    public UserDoesNotExistException(){
        super();
    }

    public UserDoesNotExistException(String msg) {
        super(msg);
    }
}
