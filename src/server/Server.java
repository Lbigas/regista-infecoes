package Server;

import Server.Exceptions.*;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.HashMap;

public class Server {
    private HashMap<String,PrintWriter> connectedClients;
    private UserList clientList;

    public Server() {
        this.connectedClients = new HashMap<String, PrintWriter>();
        this.clientList = new UserList();
        try {
            this.clientList.LoadLocalData();
        } catch (SaveFileDoesNotExistException e) {
            System.out.println("Nao foi encontrado o ficheiro de gravacao! Possivelmente ainda nao foi criado!");
        }
        try{
            this.clientList.updateNRegisters();
            this.clientList.calcBalance();
            System.out.println(this.clientList.getLastBalance());
        } catch (UserDoesNotExistException e) {
            e.printStackTrace();
        }

    }

    public UserList getClientList() {
        return clientList;
    }

    public boolean loginClient(String nick, String password, PrintWriter writer)
        throws UserAlreadyConnectedException, UserLoginInvalidException {

        if(this.connectedClients.containsKey(nick)){
            throw new UserAlreadyConnectedException(nick);
        }

        if(!this.clientList.checkLogin(nick, password)){
            throw new UserLoginInvalidException();
        }

        this.connectedClients.put(nick,writer);
        return true;


    }

    public synchronized boolean registerClient(String nick, String password, PrintWriter writer)
        throws UserAlreadyConnectedException, UserAlreadyRegisteredException {

        if(this.connectedClients.containsKey(nick)) {
            throw new UserAlreadyConnectedException();
        }
        if (this.clientList.userExists(nick)){
            throw new UserAlreadyRegisteredException(nick);
        }
        this.clientList.addUser(nick, password);

        //O cliente automaticamente faz login e encontra-se presente no sistema
        this.connectedClients.put(nick,writer);
        return true;

    }

    public void saveUserData() throws IOException {
        this.clientList.SaveLocalData();
    }

    public synchronized void disconnectClient(String nick){
        this.connectedClients.remove(nick);
    }

}
