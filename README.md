# Regista_Infecoes

## Programa Server:

Aceita a ligação de múltiplos clientes simultaneamente.
Regista o total de infecções registadas por cada cliente e envia a media de infecções para todos os clientes ligados ao servidor.

## Programa Client:

Permite realizar a ligação ao servidor, realizar registo/login, e envio/recepção do número de infecções registadas.
